package org.mule.extension.internal.dto;

public class MailInfoRequest  implements java.io.Serializable {

	private static final long serialVersionUID = -1864124344413697289L;

	private String receiver;
	private String subject;
	private String payload;
	private String imageLocation;

	public MailInfoRequest() {
		
	}
	
	public MailInfoRequest(String receiver, String subject, String payload) {
		this.receiver = receiver;
		this.subject = subject;
		this.payload = payload;
	}
	
	public String getImageLocation() {
		return imageLocation;
	}
	
	public void setImageLocation() {
		this.imageLocation = imageLocation;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}	
}